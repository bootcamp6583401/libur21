import React, { useState } from "react";
import api from "../axios";
import { User } from "../App";

export default function LoginPage({
  setUser,
  setPage,
}: {
  setUser: React.Dispatch<React.SetStateAction<User | null>>;
  setPage: React.Dispatch<
    React.SetStateAction<"LOGIN" | "REGISTER" | "WELCOME">
  >;
}) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  /* v8 ignore next 15 */
  const handleSubmit: React.FormEventHandler<HTMLFormElement> = async (e) => {
    e.preventDefault();

    const response = await api.post("/authenticate", {
      email,
      password,
    });

    setUser({ email, token: response.data.token as string });
    localStorage.setItem(
      "user",
      JSON.stringify({ email, token: response.data.token as string })
    );
    setPage("WELCOME");
  };
  return (
    <div className="min-h-screen flex flex-col justify-center items-center">
      <div className="rounded-md border border-zinc-400 shadow-lg w-[600px] max-w-full p-8">
        <h1 className="text-3xl font-semibold text-center mb-8 text-zinc-700">
          Login
        </h1>
        <form className="flex flex-col gap-4" onSubmit={handleSubmit}>
          <input
            type="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            placeholder="Your email"
            className="border-zinc-200 border p-2"
          />
          <input
            type="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
            placeholder="Password"
            className="border-zinc-200 border p-2"
          />
          <button className="bg-blue-800 text-white rounded-md px-4 py-2">
            Login
          </button>
        </form>
      </div>
          
    </div>
  );
}
