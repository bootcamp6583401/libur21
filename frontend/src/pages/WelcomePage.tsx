import React from "react";
import { User } from "../App";

export default function WelcomePage({
  setUser,
  setPage,
}: {
  setUser: React.Dispatch<React.SetStateAction<User | null>>;
  setPage: React.Dispatch<
    React.SetStateAction<"LOGIN" | "REGISTER" | "WELCOME">
  >;
}) {
  return (
    <div className="min-h-screen flex flex-col justify-center items-center">
      <div className="rounded-md border border-zinc-400 shadow-lg w-[600px] max-w-full p-8">
        <h1 className="text-3xl font-semibold text-center text-zinc-700">
          Welcome
        </h1>
        <button
          className="block mt-4 bg-blue-800 text-white rounded-md shadow-md px-4 py-2 mx-auto"
          onClick={() => {
            setPage("LOGIN");
            setUser(null);
            localStorage.removeItem("user");
          }}
        >
          Logout
        </button>
      </div>
          
    </div>
  );
}
