import { useEffect, useState } from "react";
import "./App.css";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import WelcomePage from "./pages/WelcomePage";

export type User = {
  email: string;
  token: string;
};

function App() {
  const [page, setPage] = useState<"LOGIN" | "REGISTER" | "WELCOME">(
    "REGISTER"
  );
  const [user, setUser] = useState<null | User>(null);

  useEffect(() => {
    const localStorageUser = localStorage.getItem("user");
    if (localStorageUser) {
      setUser(JSON.parse(localStorageUser));
    }
  }, []);

  useEffect(() => {
    if (page === "WELCOME" && user === null) {
      setPage("LOGIN");
    }
  }, [page]);

  return (
    <div className="">
      <nav className="flex gap-2 items-center">
        <button
          className=""
          onClick={() => {
            setPage("LOGIN");
          }}
        >
          Login
        </button>
        <button
          className=""
          onClick={() => {
            setPage("REGISTER");
          }}
        >
          Register
        </button>
        <button
          className=""
          onClick={() => {
            setPage("WELCOME");
          }}
        >
          Welcome
        </button>
      </nav>
      {page === "LOGIN" && <LoginPage setUser={setUser} setPage={setPage} />}
      {page === "REGISTER" && (
        <RegisterPage setUser={setUser} setPage={setPage} />
      )}
      {page === "WELCOME" && user !== null && (
        <WelcomePage setUser={setUser} setPage={setPage} />
      )}
    </div>
  );
}

export default App;
