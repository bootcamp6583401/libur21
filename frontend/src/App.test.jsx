import renderer from "react-test-renderer";
import { test, expect, vi } from "vitest";
import App from "./App.tsx";
import LoginPage from "./pages/LoginPage.tsx";
import RegisterPage from "./pages/RegisterPage.tsx";
import WelcomePage from "./pages/WelcomePage.tsx";

global.localStorage = {
  getItem: (name) => {
    return JSON.stringify({
      email: "test@test",
      token: "sdfsdf",
    });
  },

  setItem: (name, obj) => {},
};

function toJson(component) {
  const result = component.toJSON();
  expect(result).toBeDefined();
  expect(result).not.toBeInstanceOf(Array);
  return result;
}

test("App", () => {
  const component = renderer.create(<App />);
  let tree = toJson(component);
  expect(tree).toMatchSnapshot();
  //   expect(1).toBe(1);
});

test("LoginPage", () => {
  const component = renderer.create(
    <LoginPage setPage={() => {}} setUser={() => {}} />
  );
  let tree = toJson(component);
  expect(tree).toMatchSnapshot();
  //   expect(1).toBe(1);
});

test("RegisterPage", () => {
  const component = renderer.create(
    <RegisterPage setPage={() => {}} setUser={() => {}} />
  );
  let tree = toJson(component);

  expect(tree).toMatchSnapshot();
  //   expect(1).toBe(1);
});

test("WelcomePage", () => {
  const component = renderer.create(
    <WelcomePage setPage={() => {}} setUser={() => {}} />
  );
  let tree = toJson(component);

  expect(tree).toMatchSnapshot();
  //   expect(1).toBe(1);
});
