package tugas.libur.module21;

import org.springframework.aot.generate.Generated;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Generated
public class Module21Application {

	public static void main(String[] args) {
		SpringApplication.run(Module21Application.class, args);
	}

}
