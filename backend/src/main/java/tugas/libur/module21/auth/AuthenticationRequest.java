package tugas.libur.module21.auth;


import org.springframework.aot.generate.Generated;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
// @Builder
@Generated
public class AuthenticationRequest {
    private String email;
    private String password;
}
