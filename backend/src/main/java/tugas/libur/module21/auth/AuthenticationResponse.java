package tugas.libur.module21.auth;

import org.springframework.aot.generate.Generated;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Generated
public class AuthenticationResponse {
    

    private String token;
}
