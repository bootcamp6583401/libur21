package tugas.libur.module21.auth;

import org.springframework.aot.generate.Generated;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Generated
public class RegisterRequest {
    private String email;
    private String password;
}
