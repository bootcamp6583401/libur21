package tugas.libur.module21.config;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.cache.NullUserCache;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import tugas.libur.module21.Role;
import tugas.libur.module21.User;
import tugas.libur.module21.UserRepository;

@ContextConfiguration(classes = {ApplicationConfig.class, AuthenticationConfiguration.class})
@ExtendWith(SpringExtension.class)
class ApplicationConfigTest {
    @Autowired
    private ApplicationConfig applicationConfig;

    @MockBean
    private UserRepository userRepository;

    /**
     * Method under test: {@link ApplicationConfig#userDetailsService()}
     */
    @Test
    void testUserDetailsService() throws UsernameNotFoundException {
        
        // Arrange
        User user = new User();
        user.setEmail("jane.doe@example.org");
        user.setId(1L);
        user.setPassword("iloveyou");
        user.setRole(Role.USER);
        Optional<User> ofResult = Optional.of(user);
        UserRepository repository = mock(UserRepository.class);
        when(repository.findByEmail(Mockito.<String>any())).thenReturn(ofResult);

        // Act
        UserDetails actualLoadUserByUsernameResult = (new ApplicationConfig(repository)).userDetailsService()
                .loadUserByUsername("janedoe");

        // Assert
        verify(repository).findByEmail(Mockito.<String>any());
        assertSame(user, actualLoadUserByUsernameResult);
    }

    /**
     * Method under test: {@link ApplicationConfig#userDetailsService()}
     */
    @Test
    void testUserDetailsService2() throws UsernameNotFoundException {
     
        // Arrange
        UserRepository repository = mock(UserRepository.class);
        when(repository.findByEmail(Mockito.<String>any())).thenThrow(new UsernameNotFoundException("Msg"));

        // Act and Assert
        assertThrows(UsernameNotFoundException.class,
                () -> (new ApplicationConfig(repository)).userDetailsService().loadUserByUsername("janedoe"));
        verify(repository).findByEmail(Mockito.<String>any());
    }

    /**
     * Method under test: {@link ApplicationConfig#authenticationProvider()}
     */
    @Test
    void testAuthenticationProvider() {

        // Arrange, Act and Assert
        assertTrue(
                ((DaoAuthenticationProvider) (new ApplicationConfig(mock(UserRepository.class))).authenticationProvider())
                        .getUserCache() instanceof NullUserCache);
        assertFalse(
                ((DaoAuthenticationProvider) (new ApplicationConfig(mock(UserRepository.class))).authenticationProvider())
                        .isForcePrincipalAsString());
        assertTrue(
                ((DaoAuthenticationProvider) (new ApplicationConfig(mock(UserRepository.class))).authenticationProvider())
                        .isHideUserNotFoundExceptions());
    }

    /**
     * Method under test:
     * {@link ApplicationConfig#authenticationManager(AuthenticationConfiguration)}
     */
    @Test
    void testAuthenticationManager() throws Exception {
        // Arrange, Act and Assert
        assertTrue(applicationConfig.authenticationManager(new AuthenticationConfiguration()) instanceof ProviderManager);
    }

    /**
     * Method under test: {@link ApplicationConfig#passwordEncoder()}
     */
    @Test
    void testPasswordEncoder() {
        
        // Arrange, Act and Assert
        assertTrue((new ApplicationConfig(mock(UserRepository.class))).passwordEncoder() instanceof BCryptPasswordEncoder);
    }
}
