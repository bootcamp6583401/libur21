package tugas.libur.module21.config;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import io.jsonwebtoken.Claims;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import tugas.libur.module21.User;

@ContextConfiguration(classes = {JwtService.class})
@ExtendWith(SpringExtension.class)
class JwtServiceTest {
    @Autowired
    private JwtService jwtService;

    /**
     * Method under test: {@link JwtService#extractUsername(String)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testExtractUsername() {
    
        // Arrange and Act
        jwtService.extractUsername("ABC123");
    }

    /**
     * Method under test: {@link JwtService#extractClaim(String, Function)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testExtractClaim() {
        // Arrange
        // TODO: Populate arranged inputs
        String token = "";
        Function<Claims, Object> claimsResolver = null;

        // Act
        Object actualExtractClaimResult = this.jwtService.extractClaim(token, claimsResolver);

        // Assert
        // TODO: Add assertions on result
    }

    /**
     * Method under test: {@link JwtService#generateToken(Map, UserDetails)}
     */
    @Test
    void testGenerateToken() {
        // Arrange
        HashMap<String, Object> extraClaims = new HashMap<>();
        extraClaims.put("foo", "42");
        extraClaims.put("4e168df50a1e820e3e0b3e106604498cebbd2920dd8618e00fb512784e02dd93", "42");
        User userDetails = mock(User.class);
        when(userDetails.getUsername()).thenReturn("janedoe");

        // Act
        jwtService.generateToken(extraClaims, userDetails);

        // Assert
        verify(userDetails).getUsername();
    }

    /**
     * Method under test: {@link JwtService#generateToken(UserDetails)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testGenerateToken2() {

        // Arrange and Act
        jwtService.generateToken(null);
    }

    /**
     * Method under test: {@link JwtService#generateToken(UserDetails)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testGenerateToken3() {

        // Arrange and Act
        jwtService.generateToken(new User());
    }

    /**
     * Method under test: {@link JwtService#isTokenValid(String, UserDetails)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testIsTokenValid() {
        
        // Arrange and Act
        jwtService.isTokenValid("ABC123", new User());
    }
}
